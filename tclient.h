#include "gen/myex.h"

class TClient {
public:
	TClient();
	~TClient();
	void init();
	void start();
	void uinit();
	int requestOne(int n);
private:
	GDBusConnection *conn;
	static GMainLoop *_gLoop;
	myexnsThuanMyexIface *_proxy;
	
	static void *startGLoop(void*);
	static gboolean handle_mysignal(myexnsThuanMyexIface *object, gint in_arg);
};
