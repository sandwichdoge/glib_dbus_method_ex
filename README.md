#What

Example of remote method call using glib dbus.

- Client will send request to call addOne() on server, server adds 1 to param then return value.

- Server constantly sends signals containing an integer to client to handle.

#Dependencies

libglib2.0-dev

#01.How to build
```
cd gen
make
cd ..
make
```

#02.How to run
```
./sv.out
./cl.out
```
