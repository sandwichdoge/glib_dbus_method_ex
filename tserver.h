#include <gio/gio.h>
#include "gen/myex.h"
//Namespace=myex_ns
//Interface=thuan.myex.iface

class TServer {
public:
	TServer();
	~TServer();
	void init();
	void start();
	void sendMysignal();

private:
	static void uinit(); // Because this is called on bus_ac_cb
	static void* startGLoop(void *);
	static void bus_ac_cb(GDBusConnection *conn, const gchar *busName, gpointer userData);
	static void name_ac_cb(GDBusConnection *conn, const gchar *busName, gpointer userData);;
	static void name_lost_cb(GDBusConnection *conn, const gchar *busName, gpointer userData);
	static myexnsThuanMyexIface *_gSkeleton;
	static GMainLoop *_gLoop;

	static gboolean addOne(myexnsThuanMyexIface *object, GDBusMethodInvocation *invocation, gint in_arg, gpointer user_data);
};
