#include <pthread.h>
#include "gen/myex.h"
#include "tclient.h"

//namespace: myex_ns
//interface: thuan.myex.iface
//Basically all structs have underscores removed then converted to camelCase. E.g: myexThuanMyexIface
//And all functions are in snake_case. E.g: myex_ns_thuan_myex_iface_call_something

GMainLoop *TClient::_gLoop = NULL;

TClient::TClient()
{
	conn = NULL;
	_proxy = NULL; // Interface between server & client
}


TClient::~TClient()
{
}


void TClient::init()
{
	GError *err = NULL;
	// 1st step: get connection to dbus daemon
	conn = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &err);
	if (err) g_print("Error %s\n", err->message);

	// 2nd step: create GMainLoop for event polling/func callbacks
	_gLoop = g_main_loop_new(NULL, FALSE);

	// 3rd step: create a proxy for the dbus interface
	_proxy = myex_ns_thuan_myex_iface_proxy_new_sync(conn,
							G_DBUS_PROXY_FLAGS_NONE,
							"thuan.myex.service",
							"/thuan/myex/object/path",
							NULL, &err);
	if (err) {
		g_print("Error %s\n", err->message);
		g_error_free(err);
		uinit();
	}

	// Register callback function to handle mysignal received from server
	g_signal_connect(_proxy, "my-signal", G_CALLBACK(handle_mysignal), NULL);

	g_print("Initialized.\n");
}


void TClient::uinit()
{
	g_main_loop_quit(_gLoop);
	g_main_loop_unref(_gLoop);
	_gLoop = NULL;
}


void TClient::start()
{
	// Create a thread for glib polling
	pthread_t thread1;
	pthread_create(&thread1, NULL, startGLoop, (void*)NULL);
	g_print("Client started.\n");
}


void* TClient::startGLoop(void *args)
{
	g_main_loop_run(_gLoop);
	return NULL;
}


// Call addOne() on server, retrieve result
int TClient::requestOne(int n)
{
	gint in = (gint)n;
	gint out;
	GError *err = NULL;
	myex_ns_thuan_myex_iface_call_add_one_sync(_proxy, in, &out, NULL, &err);
	if (err) {
		g_print("%s", err->message);
		uinit(); // Uninit
	}

	return (int)out;
}


gboolean TClient::handle_mysignal(myexnsThuanMyexIface *object, gint in_arg)
{
	g_print("Received my-signal from server with value %d.\n", in_arg);
	return TRUE;
}


int main()
{
	TClient client;
	client.init();
	client.start();

	int n = 2;

	g_print("Sending [%d].\n", n);
	int result = client.requestOne(n);
	g_print("Got [%d] from server\n", result);

	while (1) {
		sleep(1);
	}

	return 0;
}
