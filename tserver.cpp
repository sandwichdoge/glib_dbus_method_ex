#include <pthread.h>
#include "tserver.h"

//Interface=thuan.myex.iface
//Namespace=myex_ns
//Abstract DBUS interface object=myexnsThuanMyexIface

GMainLoop *TServer::_gLoop = NULL;
myexnsThuanMyexIface *TServer::_gSkeleton = NULL;


TServer::TServer()
{
}

TServer::~TServer()
{
}


void TServer::init()
{
	//bus_ac_cb() will be called immediately after this
	(void) g_bus_own_name(G_BUS_TYPE_SESSION,
			"thuan.myex.service", // Bus name
			G_BUS_NAME_OWNER_FLAGS_NONE,
			&bus_ac_cb, // This func is called upon bus acquisition
			&name_ac_cb, // Callback on name acquisition
			&name_lost_cb, //Callback on name lost
			NULL,
			NULL);
	g_print("Owned name.\n");
	_gLoop = g_main_loop_new(NULL, FALSE);
}


void TServer::uinit()
{
	g_main_loop_quit(_gLoop);
	g_main_loop_unref(_gLoop);
	_gLoop = NULL;
}


void TServer::name_ac_cb(GDBusConnection *conn, const gchar *name, gpointer user_data)
{ /*Unused function*/
}

void TServer::name_lost_cb(GDBusConnection *conn, const gchar *name, gpointer user_data)
{ /*Unused function*/
}

void TServer::bus_ac_cb(GDBusConnection *conn, const gchar *name, gpointer user_data)
{
	GError *err = NULL;

	_gSkeleton = myex_ns_thuan_myex_iface_skeleton_new();

	// Got this string "handle-add-one" from generated code gen/myex.h
	g_signal_connect(_gSkeleton, "handle-add-one", G_CALLBACK(addOne), NULL);
	g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(_gSkeleton),
					conn,
					"/thuan/myex/object/path",
					&err);

	if (err != NULL) {
		g_print("Problem exporting skel %s.", err->message);
		g_main_loop_quit(_gLoop);
	}

	g_print("Exported Skeleton.\n");
}


void TServer::start()
{
	pthread_t thread1;
	int err = pthread_create(&thread1, NULL, startGLoop, (void*)NULL);

	if (err) {
		g_print("pthread creation error.\n");
		uinit();
	}
}

void* TServer::startGLoop(void *args)
{
	g_main_loop_run(_gLoop);
	return NULL;
}


// Send mysignal to client
void TServer::sendMysignal()
{
	gint in = 777;

	g_print("Sending mysignal to client with value %d.\n", in);
	myex_ns_thuan_myex_iface_emit_my_signal(_gSkeleton, in);
}


// Take value from client, increment by 1 then return it to client.
gboolean TServer::addOne(myexnsThuanMyexIface *object, GDBusMethodInvocation *invocation, gint in_arg, gpointer user_data)
{
	g_print("addOne called.\n");
	in_arg += 1; // Return value to send to client

	// Call this when you're finished, to send output argument to client
	myex_ns_thuan_myex_iface_complete_add_one(_gSkeleton, invocation, in_arg);
	
	return TRUE;
}


int main()
{
	TServer ex_server;
	ex_server.init();
	ex_server.start();
	g_print("Started server.\n");
	sleep(5);

	while (1) {
		sleep(1);
		ex_server.sendMysignal();
	}

	return 0;
}
